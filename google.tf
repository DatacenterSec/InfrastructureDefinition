provider "google" {
  credentials = "${file("~/.gcloud/account.json")}"
  project     = "${var.gcp_project}"
  region      = "${var.gcp_region}"
}
