output "control-plane-public-ipv4-addresses" {
    description = "Contains the public IPv4 addresses of the control plane servers."
    value = ["${digitalocean_droplet.control.*.ipv4_address}"]
}

output "control-plane-private-ipv4-addresses" {
    description = "Contains the private IPv4 addresses of the control plane servers."
    value = ["${digitalocean_droplet.control.*.ipv4_address_private}"]
}

output "certificate-authority-public-ipv4-address" {
    description = "Contains the public IPv4 addresses of the certificate authority server."
    value = ["${digitalocean_droplet.ca.ipv4_address}"]
}

output "certificate-authority-private-ipv4-address" {
    description = "Contains the private IPv4 addresses of the certificate authority server."
    value = ["${digitalocean_droplet.ca.ipv4_address_private}"]
}
