resource "digitalocean_ssh_key" "gitlab-terraform" {
  name       = "gitlab@terraform"
  public_key = "${file("/root/.ssh/id_ed25519.pub")}"
}
