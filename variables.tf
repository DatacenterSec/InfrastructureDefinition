variable "counts" {
  type = "map"

  default = {
    "compute" = 2
    "control" = 5
  }
}

variable "ca_size" {
  default = "s-2vcpu-2gb"
}

variable "control_size" {
  default = "s-2vcpu-2gb"
}

variable "compute_size" {
  default = "s-4vcpu-8gb"
}

variable "postgresql_size" {
  default = "s-6vcpu-16gb"
}

variable "digitalocean_region" {
  # SFO2 supports volumes
  default = "sfo2"
}

variable "domain" {}

variable "ca_shared_key" {}

variable "commit_id" {}

variable "consul_gossip_key" {}

variable "nomad_gossip_key" {}

variable "gcp_project" {}

variable "gcp_region" {}

variable "intermediate_ca_private_key" {}

variable "intermediate_ca_certificate" {}

variable "digitalocean_readonly_api_token" {}

