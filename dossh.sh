#!/bin/bash

set -e

TARGET_DROPLET=${1}

if [ ! -f key ]; then
    echo "Expected an SSH private key to live in the current directory called 'key'."
    exit 1
fi

if [ $(stat -c %a key) != 400 ]; then
    echo "Expected file permissions on the SSH private key to be 400."
    exit 1
fi

if [ -z ${TARGET_DROPLET} ]; then
    echo "You need to specify a droplet name."
    exit 1
fi

DROPLET_PUBLIC_IP=$(doctl compute droplet list $1 --no-header --format PublicIPv4)

if [ -z ${DROPLET_PUBLIC_IP} ]; then
    echo "No droplet named ${TARGET_DROPLET} found."
    exit 1
fi

ssh -i ./key root@${DROPLET_PUBLIC_IP}
