resource "digitalocean_droplet" "ca" {
  image              = "${data.digitalocean_image.certify.image}"
  size               = "${var.ca_size}"
  region             = "${var.digitalocean_region}"
  name               = "ca"
  ssh_keys           = ["${digitalocean_ssh_key.gitlab-terraform.id}"]
  private_networking = true
  tags               = ["certificate-authority"]

  connection {
    private_key = "${file("/root/.ssh/id_ed25519")}"
  }

  provisioner "file" {
    content = "${data.template_file.ca-config.rendered}"
    destination = "/etc/cfssl/config.json"
  }

  provisioner "file" {
    content = "${data.template_file.ca-server-csr.rendered}"
    destination = "/etc/cfssl/ca-server-csr.json"
  }

  provisioner "remote-exec" {
    inline = [
      "echo ${var.intermediate_ca_private_key} | base64 -d - >> /etc/cfssl/intermediate-ca-key.pem",
      "echo ${var.intermediate_ca_certificate} | base64 -d - >> /etc/cfssl/intermediate-ca.pem",
      "chown --recursive ca:ca /etc/cfssl"
    ]
  }
}

data "digitalocean_image" "certify" {
  name = "certify-${var.commit_id}"
}

data "template_file" "ca-config" {
  template = "${file("${path.module}/ca-config.json")}"

  vars = {
    ca_shared_key = "${var.ca_shared_key}"
  }
}

data "template_file" "ca-server-csr" {
  template = "${file("${path.module}/ca-server-csr.json")}"
}
