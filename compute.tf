resource "digitalocean_droplet" "compute" {
  count              = "${var.counts["compute"]}"
  image              = "${data.digitalocean_image.compute.image}"
  size               = "${var.compute_size}"
  region             = "${var.digitalocean_region}"
  name               = "compute${count.index + 1}"
  ssh_keys           = ["${digitalocean_ssh_key.gitlab-terraform.id}"]
  private_networking = true
  tags               = ["hashicorp", "compute"]

  connection {
    private_key = "${file("/root/.ssh/id_ed25519")}"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mkdir -p /run/secrets"
    ]
  }

  provisioner "file" {
    content = "${data.template_file.cfssl-config.rendered}"
    destination = "/etc/ssl/cfssl-config.json"
  }

  provisioner "file" {
    content = "${data.template_file.compute-csr.rendered}"
    destination = "/etc/ssl/csr.json"
  }

  provisioner "file" {
    content = "GOSSIP_KEY=${var.consul_gossip_key}"
    destination = "/run/secrets/consul-gossip-key"
  }

  provisioner "file" {
    content = "DIGITALOCEAN_READONLY_API_TOKEN=${var.digitalocean_readonly_api_token}"
    destination = "/run/secrets/digitalocean-readonly-api-token"
  }

  provisioner "remote-exec" {
    inline = [
      "systemctl restart consul"
    ]
  }

  provisioner "file" {
    content = "NOMAD_GOSSIP_KEY=${var.nomad_gossip_key}"
    destination = "/run/secrets/nomad-gossip-key"
  }
}

data "digitalocean_image" "compute" {
  name = "compute-${var.commit_id}"
}

data "template_file" "compute-csr" {
  template = "${file("${path.module}/compute-csr.json")}"

  vars = {
    region = "${var.digitalocean_region}"
  }
}
