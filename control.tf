resource "digitalocean_droplet" "control" {
  count              = "${var.counts["control"]}"
  image              = "${data.digitalocean_image.control.image}"
  size               = "${var.control_size}"
  region             = "${var.digitalocean_region}"
  name               = "control${count.index + 1}"
  ssh_keys           = ["${digitalocean_ssh_key.gitlab-terraform.id}"]
  private_networking = true
  tags               = ["hashicorp", "control"]

  connection {
    private_key = "${file("/root/.ssh/id_ed25519")}"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mkdir -p /run/secrets"
    ]
  }
  
  provisioner "file" {
    content = "${data.template_file.cfssl-config.rendered}"
    destination = "/etc/ssl/cfssl-config.json"
  }

  provisioner "file" {
    content = "${data.template_file.control-csr.rendered}"
    destination = "/etc/ssl/csr.json"
  }

  provisioner "file" {
    content = "GOSSIP_KEY=${var.consul_gossip_key}"
    destination = "/run/secrets/consul-gossip-key"
    
  }

  provisioner "file" {
    content = "DIGITALOCEAN_READONLY_API_TOKEN=${var.digitalocean_readonly_api_token}"
    destination = "/run/secrets/digitalocean-readonly-api-token"
  }

  provisioner "file" {
    content = "NOMAD_GOSSIP_KEY=${var.nomad_gossip_key}"
    destination = "/run/secrets/nomad-gossip-key"
  }

  provisioner "remote-exec" {
    inline = [
      "systemctl restart consul"
    ]
  }
}

data "digitalocean_image" "control" {
  name = "control-${var.commit_id}"
}

data "template_file" "control-csr" {
  template = "${file("${path.module}/control-csr.json")}"

  vars = {
    region = "${var.digitalocean_region}"
  }
}
