resource "digitalocean_droplet" "loadbalancer" {
  image              = "${data.digitalocean_image.loadbalancer.image}"
  size               = "s-2vcpu-2gb"
  region             = "${var.digitalocean_region}"
  name               = "loadbalancer"
  ssh_keys           = ["${digitalocean_ssh_key.gitlab-terraform.id}"]
  private_networking = true
  tags               = ["loadbalancer"]

  connection {
    private_key = "${file("/root/.ssh/id_ed25519")}"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mkdir -p /run/secrets"
    ]
  }

  provisioner "file" {
    content = "${data.template_file.cfssl-config.rendered}"
    destination = "/etc/ssl/cfssl-config.json"
  }

  provisioner "file" {
    content = "${data.template_file.loadbalancer-csr.rendered}"
    destination = "/etc/ssl/csr.json"
  }

  provisioner "file" {
    content = "GOSSIP_KEY=${var.consul_gossip_key}"
    destination = "/run/secrets/consul-gossip-key"
  }

  provisioner "file" {
    content = "DIGITALOCEAN_READONLY_API_TOKEN=${var.digitalocean_readonly_api_token}"
    destination = "/run/secrets/digitalocean-readonly-api-token"
  }

  provisioner "remote-exec" {
    inline = [
      "systemctl restart consul"
    ]
  }
}

resource "digitalocean_record" "loadbalancer_fqdn" {
  domain = "${var.domain}"
  type   = "A"
  ttl    = 60
  name   = "${digitalocean_droplet.loadbalancer.name}"
  value  = "${digitalocean_droplet.loadbalancer.ipv4_address}"
}

data "digitalocean_image" "loadbalancer" {
  name = "loadbalancer-${var.commit_id}"
}

data "template_file" "loadbalancer-csr" {
  template = "${file("${path.module}/loadbalancer-csr.json")}"
}
