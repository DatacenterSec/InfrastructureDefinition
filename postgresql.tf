resource "digitalocean_volume" "postgresql" {
  region                  = "${var.digitalocean_region}"
  name                    = "postgresql-data"
  size                    = 500
  initial_filesystem_type = "ext4"
  description             = "PostgreSQL Data"
}

resource "digitalocean_volume_attachment" "postgresql_attachment" {
  droplet_id  = "${digitalocean_droplet.postgresql.id}"
  volume_id   = "${digitalocean_volume.postgresql.id}"
}

resource "digitalocean_droplet" "postgresql" {
  image               = "${data.digitalocean_image.postgresql.image}"
  size                = "${var.postgresql_size}"
  region              = "${var.digitalocean_region}"
  name                = "postgresql-primary"
  ssh_keys            = ["${digitalocean_ssh_key.gitlab-terraform.id}"]
  private_networking  = true
  tags                = ["postgresql"]

  connection {
    private_key = "${file("/root/.ssh/id_ed25519")}"
  }
  
  provisioner "remote-exec" {
    inline = [
      "sudo mkdir -p /run/secrets"
    ]
  }

  provisioner "file" {
    content = "${data.template_file.cfssl-config.rendered}"
    destination = "/etc/ssl/cfssl-config.json"
  }

  provisioner "file" {
    content = "${data.template_file.postgresql-csr.rendered}"
    destination = "/etc/ssl/csr.json"
  }

  provisioner "file" {
    content = "GOSSIP_KEY=${var.consul_gossip_key}"
    destination = "/run/secrets/consul-gossip-key"
  }

  provisioner "file" {
    content = "DIGITALOCEAN_READONLY_API_TOKEN=${var.digitalocean_readonly_api_token}"
    destination = "/run/secrets/digitalocean-readonly-api-token"
  }

  provisioner "remote-exec" {
    inline = [
      "systemctl restart consul"
    ]
  }
}

data "digitalocean_image" "postgresql" {
  name = "postgresql-${var.commit_id}"
}

data "template_file" "postgresql-csr" {
  template = "${file("${path.module}/postgresql-csr.json")}"
}
