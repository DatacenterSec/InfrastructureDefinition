terraform {
  backend "gcs" {
    prefix      = "terraform/state"
    credentials = "~/.gcloud/account.json"
  }
}
