data "template_file" "cfssl-config" {
  template = "${file("${path.module}/cfssl-config.json")}"

  vars = {
    ca_shared_key = "${var.ca_shared_key}"
  }
}
